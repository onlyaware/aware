package com.onlyxiahui.aware.common.auth.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.onlyxiahui.aware.common.auth.type.PermissionType;

/**
 * date 2018-07-04 15:28:04<br>
 * description
 * 
 * @author XiaHui<br>
 * @since
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PermissionMapping {

	PermissionType type() default PermissionType.grant;

}
