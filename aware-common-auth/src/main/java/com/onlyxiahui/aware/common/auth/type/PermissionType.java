package com.onlyxiahui.aware.common.auth.type;

/**
 * Description 通过类型<br>
 * Date 2020-06-07 17:55:28<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public enum PermissionType {
	/**
	 * 忽略，无需拦截
	 */
	skip,
	/**
	 * 登录方可通过
	 */
	auth,
	/**
	 * 有权限方可通过
	 */
	grant
}
