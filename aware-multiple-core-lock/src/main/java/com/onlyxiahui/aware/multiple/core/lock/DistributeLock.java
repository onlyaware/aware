package com.onlyxiahui.aware.multiple.core.lock;

/**
 * Description <br>
 * Date 2020-12-21 15:35:23<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public interface DistributeLock {
	/**
	 * 尝试加锁
	 *
	 * @param lockKey 锁的key
	 * @return true 成功 false 失败
	 * @since 1.0.0
	 */
	boolean lock(String lockKey);

	/**
	 * 尝试加锁 (requestKey相等 可重入)
	 *
	 * @param lockKey           锁的key
	 * @param expireMillisecond 过期时间 单位：秒
	 * @return true 成功 false 失败
	 * @since 1.0.0
	 */
	boolean lock(String lockKey, int expireMillisecond);

	/**
	 * 尝试加锁 (requestKey相等 可重入)
	 *
	 * @param lockKey    锁的key
	 * @param requestKey 请求Key
	 * @return true 成功 false 失败
	 * @since 1.0.0
	 */
	boolean lock(String lockKey, String requestKey);

	/**
	 * 尝试加锁 (requestKey相等 可重入)
	 *
	 * @param lockKey           锁的key
	 * @param requestKey        请求Key
	 * @param expireMillisecond 过期时间 单位：秒
	 * @return true 成功 false 失败
	 * @since 1.0.0
	 */
	boolean lock(String lockKey, String requestKey, int expireMillisecond);

	/**
	 * 尝试加锁，失败自动重试 会阻塞当前线程
	 * 
	 * @param lockKey 锁的key
	 * @return true 成功 false 失败
	 * @since 1.0.0
	 */
	boolean lockAndRetry(String lockKey);

	/**
	 * 尝试加锁，失败自动重试 会阻塞当前线程 (requestKey相等 可重入)
	 *
	 * @param lockKey    锁的key
	 * @param requestKey 请求Key
	 * @return true 成功 false 失败
	 * @since 1.0.0
	 */
	boolean lockAndRetry(String lockKey, String requestKey);

	/**
	 * 尝试加锁 (requestKey相等 可重入)
	 *
	 * @param lockKey           锁的key
	 * @param expireMillisecond 过期时间 单位：秒
	 * @return true 成功 false 失败
	 * @since 1.0.0
	 */
	boolean lockAndRetry(String lockKey, int expireMillisecond);

	/**
	 * 尝试加锁 (requestKey相等 可重入)
	 *
	 * @param lockKey           锁的key
	 * @param expireMillisecond 过期时间 单位：秒
	 * @param retryCount        重试次数
	 * @return true 成功 false 失败
	 * @since 1.0.0
	 */
	boolean lockAndRetry(String lockKey, int expireMillisecond, int retryCount);

	/**
	 * 尝试加锁 (requestKey相等 可重入)
	 *
	 * @param lockKey           锁的key
	 * @param requestKey        请求Key
	 * @param expireMillisecond 过期时间 单位：秒
	 * @return true 成功 false 失败
	 * @since 1.0.0
	 */
	boolean lockAndRetry(String lockKey, String requestKey, int expireMillisecond);

	/**
	 * 尝试加锁 (requestKey相等 可重入)
	 *
	 * @param lockKey           锁的key
	 * @param requestKey        请求Key
	 * @param expireMillisecond 过期时间 单位：秒
	 * @param retryCount        重试次数
	 * @return true 成功 false 失败
	 * @since 1.0.0
	 */
	boolean lockAndRetry(String lockKey, String requestKey, int expireMillisecond, int retryCount);

	/**
	 * 尝试加锁 (requestKey相等 可重入) <br>
	 * 
	 * @param lockKey                  锁的key
	 * @param requestKey               请求Key
	 * @param expireMillisecond        过期时间 单位：秒
	 * @param retryCount               重试次数
	 * @param retryIntervalMillisecond 重试时间 单位：秒
	 * @return true 成功 false 失败
	 * @since 1.0.0
	 */
	boolean lockAndRetry(String lockKey, String requestKey, int expireMillisecond, int retryCount, int retryIntervalMillisecond);

	/**
	 * 释放锁
	 *
	 * @param lockKey    锁的key
	 * @param requestKey 用户ID
	 * @return true 释放自己所持有的锁 成功 false 释放自己所持有的锁 失败
	 */
	boolean unLock(String lockKey);

	/**
	 * 
	 * Description <br>
	 * Date 2021-02-01 15:06:28<br>
	 * 
	 * @param lockKey
	 * @param requestKey
	 * @return
	 * @since 1.0.0
	 */
	boolean unLock(String lockKey, String requestKey);
}
