package com.onlyxiahui.aware.basic.work.business.error;

/**
 * 系统常量
 * 
 * @author XiaHui
 * 
 */
public enum ErrorCode {

	/**
	 * 系统通用异常错误
	 */
	system("1", "."),
	/**
	 * 数据验证错误
	 */
	validate("2", "."),
	/**
	 * 业务错误
	 */
	business("3", ".");

	private String value;
	private String separator;

	private ErrorCode(String value, String separator) {
		this.value = value;
		this.separator = separator;
	}

	public String value() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String code(String code) {
		StringBuilder sb = new StringBuilder();
		sb.append(this.value);
		sb.append(separator);
		sb.append(code);
		return sb.toString();
	}

	public static void setSeparator(String separator) {
		for (ErrorCode v : ErrorCode.values()) {
			v.separator = separator;
		}
	}
}
