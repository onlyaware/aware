package com.onlyxiahui.aware.basic.work.business.error;

/**
 * Date 2019-01-19 20:45:52<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public enum SystemError {

	/**
	 * 服务内部错误（例如服务不可用）
	 */
	SYSTEM_EXCEPTIONS("000", "系统异常，未知错误"),
	/**
	 * 无效请求或接口不存在
	 */
	INVALID_REQUEST("001", "无效请求或接口不存在"),
	/**
	 * 拒绝访问（包括IP限制等）
	 */
	ACCESS_DENIED("002", "拒绝访问！"),
	/**
	 * 认证失败（包括 appId/appKey/token/scope 错误）
	 */
	AUTHENTICATION_FAILED("003", "认证失败！"),
	/**
	 * 签名错误（签名校验失败）
	 */
	SIGNATURE_ERROR("004", "签名错误！"),
	/**
	 * 必填参数缺失
	 */
	PARAMETER_MISSING("005", "必填参数缺失！"),
	/**
	 * 参数格式错误
	 */
	PARAMETER_ERROR("006", "参数格式错误！"),
	/**
	 * 请求超时（通常是内部调用超时）
	 */
	REQUEST_TIMEOUT("007", "请求超时（通常是内部调用超时）！"),
	/**
	 * 连接过多（通常是熔断限流引起）
	 */
	TOO_MANY_CONNECTIONS("008", "连接过多（通常是熔断限流引起）！"),
	/**
	 * 网络异常（网络类型错误）
	 */
	NETWORK_ERROR("009", "网络异常（网络类型错误）！"),
	/**
	 * RPC 服务调用错误
	 */
	RPC_SERVICE_ERROR("010", "RPC 服务调用错误！"),
	/**
	 * 数据库错误
	 */
	DATABASE_ERROR("011", "数据库错误！"),
	/**
	 * 请求数据验证失败
	 */
	PARAMETER_VALID_ERROR("012", "请求数据验证失败！"),
	/**
	 * 请求数据不能为空
	 */
	PARAMETER_REQUIRED("013", "请求数据不能为空！"),
	/**
	 * 触发熔断机制，执行回退策略。
	 */
	FALLBACK("014", "触发熔断机制，执行回退策略。"),
	/**
	 * 调用其他服务接口调用异常
	 */
	OTHER_API_EXCEPTIONS("015", "调用其他服务接口调用异常");

	private String value;
	private String message;

	private SystemError(String value, String message) {
		this.message = message;
		this.value = value;
	}

	public String value() {
		return ErrorCode.system.code(this.value);
	}

	public String message() {
		return this.message;
	}
}
