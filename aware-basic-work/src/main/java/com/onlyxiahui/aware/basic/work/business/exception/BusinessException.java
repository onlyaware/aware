package com.onlyxiahui.aware.basic.work.business.exception;

/**
 * 
 * Date 2019-04-20 09:57:01<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class BusinessException extends RuntimeException {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 异常代码
	 */
	protected String code;
	/**
	 * 给用户的友好提示信息
	 */
	protected String prompt;

	private void setCode(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public String getPrompt() {
		return prompt;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	/**
	 * 
	 * @param code
	 * @param message 开发人员的具体错误提示，以帮助排查问题
	 */
	public BusinessException(String code, String message) {
		super(message);
		setCode(code);
	}

	/**
	 * 
	 * @param code
	 * @param message 开发人员的具体错误提示，以帮助排查问题
	 * @param prompt  给用户的友好提示信息
	 */
	public BusinessException(String code, String message, String prompt) {
		super(message);
		setCode(code);
		setPrompt(prompt);
	}

	/**
	 * 
	 * @param code
	 * @param message 开发人员的具体错误提示，以帮助排查问题
	 * @param prompt  给用户的友好提示信息
	 * @param cause   具体异常
	 */
	public BusinessException(String code, String message, String prompt, Throwable cause) {
		super(message, cause);
		setCode(code);
		setPrompt(prompt);
	}
}
