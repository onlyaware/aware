package com.onlyxiahui.aware.basic.work.business.error;

/**
 * Date 2019-01-19 20:51:26<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public enum ValidateError {
	/**
	 * 属性错误：001{001：不能为空对象、002:不能空字符串、003:不能空对象和空字符串、004：数据类型错误、005：格式错误、006：范围错误(不合法)、007：不匹配}
	 */

	isNull("001", "不能为空对象!"),
	isEmpty("002", "不能空字符串!"),
	isBlank("003", "不能空对象和空字符串!"),
	typeError("004", "数据类型错误!"),
	formatError("005", "格式错误!"),
	rangeError("006", "范围错误(不合法)!"),
	matchingError("007", "不匹配!");

	private String value;
	private String message;

	private ValidateError(String value, String message) {
		this.message = message;
		this.value = value;
	}

	public String value() {
		return ErrorCode.validate.code(this.value);
	}

	public String message() {
		return this.message;
	}
}
