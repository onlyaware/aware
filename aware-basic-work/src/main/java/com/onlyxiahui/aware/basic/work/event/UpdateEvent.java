package com.onlyxiahui.aware.basic.work.event;

/**
 * 
 * Description <br>
 * Date 2019-05-05 09:31:14<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @param <T>
 * @param <D>
 * @since 1.0.0
 */

public interface UpdateEvent<T, D> {

	public void event(T type, D data);
}
