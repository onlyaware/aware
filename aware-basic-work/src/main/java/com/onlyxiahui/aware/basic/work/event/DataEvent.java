package com.onlyxiahui.aware.basic.work.event;

/**
 * Date 2019-01-20 10:24:57<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public interface DataEvent<T> {

	public void event(T data);
}
