
package com.onlyxiahui.aware.basic.base.generate;

/**
 * Description <br>
 * Date 2020-04-17 09:49:45<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public interface KeyGenerate {

	/**
	 * 
	 * Description <br>
	 * Date 2020-11-22 21:14:10<br>
	 * 
	 * @return
	 * @since 1.0.0
	 */
	String getId();
}
