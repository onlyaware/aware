package com.onlyxiahui.aware.extend.shardingsphere.algorithm;

import java.util.Collection;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import com.onlyxiahui.aware.extend.shardingsphere.algorithm.util.ShardingHandlerUtil;

/**
 * int 取模<br>
 * Date 2019-08-26 10:57:11<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class ModulusIntegerPreciseShardingAlgorithm implements PreciseShardingAlgorithm<Integer> {

	@Override
	public String doSharding(
			final Collection<String> databaseNames,
			final PreciseShardingValue<Integer> shardingValue) {
		// 当前数据节点数量
		int size = databaseNames.size();
		if (size == 1) {
			return databaseNames.iterator().next();
		}

		Integer value = shardingValue.getValue();
		// 计算当前分片值所在的节点号
		int nodeNumber = Math.floorMod(value, size);
		// 找到匹配的数据库名称并返回
		for (String databaseName : databaseNames) {
			if (ShardingHandlerUtil.eq(databaseName, nodeNumber)) {
				return databaseName;
			}
		}
		throw new RuntimeException("数据分片异常");
	}
}
