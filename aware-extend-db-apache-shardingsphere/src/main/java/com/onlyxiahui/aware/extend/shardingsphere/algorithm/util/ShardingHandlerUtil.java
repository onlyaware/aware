package com.onlyxiahui.aware.extend.shardingsphere.algorithm.util;

/**
 * 
 * <br>
 * Date 2019-11-19 10:30:42<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ShardingHandlerUtil {

	public static boolean eq(String databaseName, int nodeNumber) {
		String no = nodeNumber + "";
		String nameNumber = getEndNumber(databaseName);
		return no.equals(nameNumber);
	}

	public static boolean eq(String databaseName, long nodeNumber) {
		String no = nodeNumber + "";
		String nameNumber = getEndNumber(databaseName);
		return no.equals(nameNumber);
	}

	public static String getEndNumber(String text) {
		String no = "";
		if (text != null && !text.isEmpty()) {
			int length = text.length();
			int end = length - 1;
			for (int i = end; i >= 0; i--) {
				char temp = text.charAt(i);
				if (temp > '9' || temp < '0') {
					no = text.substring(i + 1, length);
					break;
				}
			}
		}
		return no;
	}
}
