package com.globalegrow.common.shardingsphere.algorithm;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import com.google.common.hash.Hashing;

/**
 * 
 * <br>
 * Date 2019-11-22 08:32:14<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ConsistentHashTest {

	public static void main(String[] args) {
//		int size = 120;
//		String text = "0232";
//		int nodeNumber = Hashing.consistentHash(Hashing.sha256().hashString(text, StandardCharsets.UTF_8), size);
//		System.out.println(nodeNumber);
		test();
	}

	public static void test() {
		int size1 = 100;
		int size2 = 110;
		Map<String, Integer> map = new HashMap<>();
		for (int i = 0; i < 10000; i++) {
			String key = "0" + i;
			int nodeNumber = Hashing.consistentHash(Hashing.sha256().hashString(key, StandardCharsets.UTF_8), size1);
			map.put(key, nodeNumber);
		}

		for (int i = 0; i < 10000; i++) {
			String key = "0" + i;

			int nodeNumber = Hashing.consistentHash(Hashing.sha256().hashString(key, StandardCharsets.UTF_8), size2);
			int n = map.getOrDefault(key, -1);
			if (nodeNumber != n) {
				System.out.println(key + ":false");
			}
		}
	}
}
