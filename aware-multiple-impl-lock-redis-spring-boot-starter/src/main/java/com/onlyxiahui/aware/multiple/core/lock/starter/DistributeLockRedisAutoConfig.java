package com.onlyxiahui.aware.multiple.core.lock.starter;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.onlyxiahui.aware.multiple.core.lock.DistributeLock;
import com.onlyxiahui.aware.multiple.core.lock.impl.DistributeLockRedis;

/**
 * Description <br>
 * Date 2021-02-01 15:08:18<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@Configuration
public class DistributeLockRedisAutoConfig {

	@Bean
	@ConditionalOnMissingBean
	public DistributeLock distributeLock() {
		DistributeLockRedis bean = new DistributeLockRedis();
		return bean;
	}
}
