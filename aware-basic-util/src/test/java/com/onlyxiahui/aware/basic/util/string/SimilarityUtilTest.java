package com.onlyxiahui.aware.basic.util.string;

/**
 * Description <br>
 * Date 2020-12-30 16:49:31<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class SimilarityUtilTest {

	public static void main(String[] args) {
		String s1 = "我是中国人";
		String s3 = "中国人";
		String s4 = "我是中国人";
		String s2 = "66666";
		String s5 = "我人";
		String s6 = "中人";
		System.out.println("==相似度===" + SimilarityUtil.similarity(s1, s2));
		System.out.println("==相似度===" + SimilarityUtil.similarity(s1, s4));
		System.out.println("==相似度===" + SimilarityUtil.similarity(s1, s3));
		System.out.println("==相似度===" + SimilarityUtil.similarity(s1, s5));
		System.out.println("==相似度===" + SimilarityUtil.similarity(s1, s6));
		System.out.println("==相似度===" + SimilarityUtil.similarity(null, null));
		System.out.println("==相似度===" + SimilarityUtil.similarity(s1, null));
	}
}
