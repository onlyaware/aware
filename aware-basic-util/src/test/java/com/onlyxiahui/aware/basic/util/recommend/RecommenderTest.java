package com.onlyxiahui.aware.basic.util.recommend;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.onlyxiahui.aware.basic.util.recommend.bean.ItemData;

/**
 * Description <br>
 * Date 2020-12-30 15:27:43<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class RecommenderTest {

	public static void main(String[] arg) {
//		List<ItemData> list = null;
		Map<String, List<ItemData>> referencesMap = new HashMap<>();
		Map<String, List<ItemData>> targetsMap = new HashMap<>();

//		String key="u001";
//		list=new ArrayList<>();
//		
//		list.add(new ItemData("name",0.5));
//		
//		referencesMap.put(key, list);

		Recommender r = new Recommender();
		Map<String, Double> recommendMap = r.recommend(referencesMap, targetsMap);
		List<Entry<String, Double>> list = new ArrayList<Entry<String, Double>>(recommendMap.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
			// 升序排序
			public int compare(Entry<String, Double> o1, Entry<String, Double> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});

		for (Entry<String, Double> e : list) {
			System.out.println(e.getKey() + ":" + e.getValue());
		}
	}
}
