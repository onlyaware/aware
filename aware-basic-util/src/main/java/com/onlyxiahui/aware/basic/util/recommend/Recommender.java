package com.onlyxiahui.aware.basic.util.recommend;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.onlyxiahui.aware.basic.util.recommend.bean.ItemData;

/**
 * Description <br>
 * Date 2020-12-30 14:53:55<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class Recommender {

	private Algorithm algorithm = new Algorithm();

	public Map<String, Double> recommend(Map<String, List<ItemData>> referencesMap, Map<String, List<ItemData>> targetsMap) {
		Map<String, Double> map = new HashMap<>();

		if (referencesMap.isEmpty()) {
			targetsMap.forEach((tk, tl) -> {
				Double r = map.get(tk);
				if (r == null) {
					r = 0.0D;
				}
				Double correlation = 0.0D;
				if (null != tl && !tl.isEmpty()) {
					correlation = tl.stream().mapToDouble(m -> m.getRate()).sum();
				}
				Double sum = r + correlation;
				map.put(tk, sum);
			});
		} else {
			targetsMap.forEach((tk, tl) -> {
				referencesMap.forEach((rk, rl) -> {
					if (!rk.equals(tk)) {
						Double r = map.get(tk);
						if (r == null) {
							r = 0.0D;
						}
						Double correlation = algorithm.correlation(tl, rl);
						Double sum = r + correlation;
						map.put(tk, sum);
					}
				});
			});
		}
		return map;
	}
}
