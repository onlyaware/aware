package com.onlyxiahui.aware.basic.util.recommend.bean;

/**
 * Description <br>
 * Date 2020-12-30 14:53:07<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ItemData {

	private String key;
	private double rate;

	public ItemData() {
	}

	public ItemData(String key, double rate) {
		this.key = key;
		this.rate = rate;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public double getRate() {
		return rate;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

}
