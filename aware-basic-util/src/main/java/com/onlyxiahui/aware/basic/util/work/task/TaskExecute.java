package com.onlyxiahui.aware.basic.util.work.task;

/**
 * 
 * Description <br>
 * Date 2014-09-12 14:21:15<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public interface TaskExecute {

	public void execute();
}
