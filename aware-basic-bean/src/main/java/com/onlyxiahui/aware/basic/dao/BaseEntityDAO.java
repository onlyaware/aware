package com.onlyxiahui.aware.basic.dao;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;

import com.onlyxiahui.common.data.common.data.Page;
import com.onlyxiahui.extend.query.hibernate.AbstractEntityDAO;
import com.onlyxiahui.extend.query.hibernate.QueryContext;
import com.onlyxiahui.extend.query.hibernate.QueryWrapper;
import com.onlyxiahui.extend.query.hibernate.syntax.data.QueryHandleData;
import com.onlyxiahui.extend.query.hibernate.syntax.data.QueryHandleUtil;
import com.onlyxiahui.extend.query.hibernate.syntax.util.UpdateHqlUtil;
import com.onlyxiahui.extend.query.hibernate.util.DbReflectUtil;
import com.onlyxiahui.extend.query.hibernate.util.QueryUtil;
import com.onlyxiahui.extend.query.page.QueryPage;

/**
 * 
 * Date 2019-01-07 21:43:56<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class BaseEntityDAO<T> extends AbstractEntityDAO<T> {

	@Autowired
	public void setSuperSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
		HibernateTemplate t = this.getHibernateTemplate();
		if (null != t) {
			t.setCheckWriteOperations(false);
		}
	}

	@Autowired
	@Override
	public void setQueryContext(QueryContext queryContext) {
		super.setQueryContext(queryContext);
	}

	@Override
	public int updateSelective(Serializable id, QueryWrapper qw) {
		int count = 0;
		Class<T> entityClass = (Class<T>) getEntityClass();
		if (null != entityClass) {
			String entityName = entityClass.getName();

			String idName = "id";
			Field field = DbReflectUtil.getField(entityClass, Object.class, Id.class);
			if (null != field) {
				Column c = field.getAnnotation(Column.class);
				idName = (null == c || c.name() == null) ? field.getName() : c.name();
				qw.addParameter(idName, id);

				String hql = UpdateHqlUtil.getUpdateHql(entityName, idName, qw);
				count = this.executeHql(hql, qw);
			}
		}
		return count;
	}

	public List<T> list(Object query, Page page) {
		QueryHandleData queryHandleData = QueryHandleUtil.get(query);
		QueryWrapper queryWrapper = QueryUtil.getQueryWrapperType(query, queryHandleData.getOptions());
		boolean hasPage = null != page;
		if (hasPage) {
			QueryPage queryPage = queryWrapper.setPage(page.getNumber(), page.getSize());
			List<T> list = list(queryWrapper, queryHandleData);
			page.setTotalCount(queryPage.getTotalCount());
			page.setTotalPage(queryPage.getTotalPage());
			return list;
		} else {
			List<T> list = list(queryWrapper, queryHandleData);
			return list;
		}
	}
}
