package org.hibernate.cfg.annotations;

import java.util.List;

import javax.persistence.Entity;

import org.hibernate.annotations.common.reflection.XClass;
import org.hibernate.boot.spi.InFlightMetadataCollector;
import org.hibernate.boot.spi.MetadataBuildingContext;
import org.hibernate.cfg.UniqueConstraintHolder;
import org.hibernate.mapping.PersistentClass;

import com.onlyxiahui.extend.hibernate.CommentBinder;

/**
 * Description <br>
 * Date 2020-12-26 09:35:01<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class AwareEntityBinder extends EntityBinder {

	XClass annotatedClass;
	PersistentClass persistentClass;

	@SuppressWarnings("deprecation")
	public AwareEntityBinder(
			Entity ejb3Ann,
			org.hibernate.annotations.Entity hibAnn,
			XClass annotatedClass,
			PersistentClass persistentClass,
			MetadataBuildingContext context) {
		super(ejb3Ann,
				hibAnn,
				annotatedClass,
				persistentClass,
				context);
		this.annotatedClass = annotatedClass;
		this.persistentClass = persistentClass;
	}

	public void bindTable(
			String schema,
			String catalog,
			String tableName,
			List<UniqueConstraintHolder> uniqueConstraints,
			String constraints,
			InFlightMetadataCollector.EntityTableXref denormalizedSuperTableXref) {
		super.bindTable(schema, catalog, tableName, uniqueConstraints, constraints, denormalizedSuperTableXref);
		// TODO
		CommentBinder.bindTableComment(annotatedClass, persistentClass);
	}
}
