package org.hibernate.cfg;

import org.hibernate.annotations.common.reflection.XProperty;
import org.hibernate.boot.spi.MetadataBuildingContext;
import org.hibernate.cfg.annotations.EntityBinder;
import org.hibernate.cfg.annotations.Nullability;

import com.onlyxiahui.extend.hibernate.CommentBinder;

/**
 * Description <br>
 * Date 2020-12-26 09:35:39<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class AwareColumnsBuilder extends ColumnsBuilder {
	private XProperty property;

	public AwareColumnsBuilder(PropertyHolder propertyHolder, Nullability nullability, XProperty property, PropertyData inferredData, EntityBinder entityBinder, MetadataBuildingContext buildingContext) {
		super(propertyHolder, nullability, property, inferredData, entityBinder, buildingContext);
		this.property = property;
	}

	public ColumnsBuilder extractMetadata() {
		Ejb3Column[] columns = this.getColumns();
		CommentBinder.bindColumnComment(property, columns);
		return super.extractMetadata();
	}
}
