package com.onlyxiahui.aware.basic.extend.jackson.databind;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * 
 * Description 时间格式反序列化 <br>
 * Date 2019-04-29 09:26:46<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class JacksonLocalDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {

	@Override
	public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
		LocalDateTime localDateTime = null;
		String text = jsonParser.getText();
		if (null != text) {
			String date = LocalDateUtil.allToDateTimeMillisecond(text);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(LocalDateUtil.FORMAT_DATE_TIME_MILLISECOND);
			localDateTime = LocalDateTime.parse(date, formatter);
		}
		return localDateTime;
	}
}
