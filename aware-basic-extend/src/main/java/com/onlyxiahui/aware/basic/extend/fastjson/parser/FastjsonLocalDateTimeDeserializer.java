package com.onlyxiahui.aware.basic.extend.fastjson.parser;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * Description <br>
 * Date 2019-06-20 22:41:30<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class FastjsonLocalDateTimeDeserializer implements ObjectDeserializer {

	@SuppressWarnings("unchecked")
	@Override
	public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
		// 如果是字符串格式
		String text = parser.getLexer().stringVal();
		parser.getLexer().nextToken();

		LocalDateTime localDateTime = null;
		if (null != text) {
			String date = LocalDateUtil.allToDateTimeMillisecond(text);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(LocalDateUtil.FORMAT_DATE_TIME_MILLISECOND);
			localDateTime = LocalDateTime.parse(date, formatter);
		}
		return (T) localDateTime;
	}

	@Override
	public int getFastMatchToken() {
		// TODO Auto-generated method stub
		return 0;
	}
}
