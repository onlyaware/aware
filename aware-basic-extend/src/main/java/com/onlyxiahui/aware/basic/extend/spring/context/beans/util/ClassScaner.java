package com.onlyxiahui.aware.basic.extend.spring.context.beans.util;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

/**
 * Date 2019-03-10 13:43:03<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public class ClassScaner {

	private ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
	private final List<TypeFilter> includeFilters = new LinkedList<TypeFilter>();
	private final List<TypeFilter> excludeFilters = new LinkedList<TypeFilter>();
	private MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(this.resourcePatternResolver);

	public ClassScaner() {

	}

	public final ResourceLoader getResourceLoader() {
		return this.resourcePatternResolver;
	}

	public void addIncludeFilter(TypeFilter includeFilter) {
		this.includeFilters.add(includeFilter);
	}

	public void addExcludeFilter(TypeFilter excludeFilter) {
		this.excludeFilters.add(0, excludeFilter);
	}

	public void resetFilters() {
		this.includeFilters.clear();
		this.excludeFilters.clear();
	}

	public Set<Class<?>> doScan(String basePackage) {
		Set<Class<?>> classes = new HashSet<Class<?>>();
		try {
			String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + org.springframework.util.ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(basePackage))
					+ "/**/*.class";
			Resource[] resources = this.resourcePatternResolver.getResources(packageSearchPath);

			for (int i = 0; i < resources.length; i++) {
				Resource resource = resources[i];
				if (resource.isReadable()) {
					MetadataReader metadataReader = this.metadataReaderFactory.getMetadataReader(resource);
					// boolean isEmpty = (includeFilters.size() == 0 && excludeFilters.size() == 0);
					// if (isEmpty || matches(metadataReader)) {
					if (matches(metadataReader)) {
						String className = metadataReader.getClassMetadata().getClassName();
						boolean isPresent = ClassUtils.isPresent(className, ClassScaner.class.getClassLoader());
						if (isPresent) {
							Class<?> classType = getClassByName(className);
							if (null != classType) {
								classes.add(classType);
							}
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return classes;
	}

	public Class<?> getClassByName(String className) {
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean matches(MetadataReader metadataReader) {

		for (TypeFilter tf : this.excludeFilters) {
			try {
				if (tf.match(metadataReader, this.metadataReaderFactory)) {
					return false;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (includeFilters.isEmpty()) {
			return true;
		}

		for (TypeFilter tf : this.includeFilters) {
			try {
				if (tf.match(metadataReader, this.metadataReaderFactory)) {
					return true;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	public static Set<Class<?>> scan(String... basePackages) {
		ClassScaner cs = new ClassScaner();
		Set<Class<?>> classes = new HashSet<Class<?>>();
		for (String s : basePackages) {
			classes.addAll(cs.doScan(s));
		}
		return classes;
	}

	public static Set<Class<?>> scan(List<String> basePackages) {
		ClassScaner cs = new ClassScaner();
		Set<Class<?>> classes = new HashSet<Class<?>>();
		for (String s : basePackages) {
			classes.addAll(cs.doScan(s));
		}
		return classes;
	}

	public static Set<Class<?>> scan(String basePackage) {
		ClassScaner cs = new ClassScaner();
		return cs.doScan(basePackage);
	}

	@SuppressWarnings("unchecked")
	public static Set<Class<?>> scan(String basePackage, Class<? extends Annotation>... annotations) {
		ClassScaner cs = new ClassScaner();
		for (Class<? extends Annotation> anno : annotations) {
			cs.addIncludeFilter(new AnnotationTypeFilter(anno));
		}
		return cs.doScan(basePackage);
	}

	@SuppressWarnings({ "unchecked" })
	public static Set<Class<?>> scan(String[] basePackages, Class<? extends Annotation>... annotations) {
		ClassScaner cs = new ClassScaner();
		for (Class<? extends Annotation> anno : annotations) {
			cs.addIncludeFilter(new AnnotationTypeFilter(anno));
		}
		Set<Class<?>> classes = new HashSet<Class<?>>();
		for (String s : basePackages) {
			classes.addAll(cs.doScan(s));
		}
		return classes;
	}
}
