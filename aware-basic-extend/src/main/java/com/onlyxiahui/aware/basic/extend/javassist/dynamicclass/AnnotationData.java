package com.onlyxiahui.aware.basic.extend.javassist.dynamicclass;

import java.util.List;

/**
 * 
 * Description <br>
 * Date 2020-04-10 15:37:38<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class AnnotationData {

	private String name;
	private List<AnnotationPropertyData> properties;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AnnotationPropertyData> getProperties() {
		return properties;
	}

	public void setProperties(List<AnnotationPropertyData> properties) {
		this.properties = properties;
	}
}
