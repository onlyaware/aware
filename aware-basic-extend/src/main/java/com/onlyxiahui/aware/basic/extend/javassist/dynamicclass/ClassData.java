package com.onlyxiahui.aware.basic.extend.javassist.dynamicclass;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * 
 * Description <br>
 * Date 2020-04-10 15:37:38<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class ClassData {

	private String className;
	private String classSimpleName;

	private List<Property> properties;

	private List<Annotation> annotations;

	private List<AnnotationData> annotationDatas;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getClassSimpleName() {
		return classSimpleName;
	}

	public void setClassSimpleName(String classSimpleName) {
		this.classSimpleName = classSimpleName;
	}

	public List<Property> getProperties() {
		return properties;
	}

	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

	public List<Annotation> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(List<Annotation> annotations) {
		this.annotations = annotations;
	}

	public List<AnnotationData> getAnnotationDatas() {
		return annotationDatas;
	}

	public void setAnnotationDatas(List<AnnotationData> annotationDatas) {
		this.annotationDatas = annotationDatas;
	}
}
