package com.onlyxiahui.aware.basic.extend.jackson.databind;

import java.io.IOException;
import java.sql.Timestamp;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.onlyxiahui.common.utils.base.util.time.DateUtil;
import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * 
 * Description 时间格式序列化 <br>
 * Date 2019-04-29 09:25:18<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class JacksonTimestampSerializer extends JsonSerializer<Timestamp> {
	@Override
	public void serialize(Timestamp value, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException {
		if (null != value) {
			String date = DateUtil.format(value, LocalDateUtil.FORMAT_DATE_TIME_MILLISECOND);
			jsonGenerator.writeString(date);
		}
	}
}
