package com.onlyxiahui.aware.basic.extend.spring.core.convert;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.core.convert.converter.Converter;

import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * Description <br>
 * Date 2019-05-21 14:51:31<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class LocalDateDeserializerConverter implements Converter<String, LocalDate> {

	@Override
	public LocalDate convert(String text) {
		LocalDate localDate = null;
		if (null != text && !text.isEmpty()) {
			text = LocalDateUtil.allToDateTimeMillisecond(text);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(LocalDateUtil.FORMAT_DATE_TIME_MILLISECOND);
			LocalDateTime localDateTime = LocalDateTime.parse(text, formatter);
			localDate = localDateTime.toLocalDate();
		}
		return localDate;
	}
}
