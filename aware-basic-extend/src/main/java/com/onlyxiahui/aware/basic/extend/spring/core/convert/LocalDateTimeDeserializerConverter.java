package com.onlyxiahui.aware.basic.extend.spring.core.convert;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.core.convert.converter.Converter;

import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * Description <br>
 * Date 2019-05-21 14:51:31<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class LocalDateTimeDeserializerConverter implements Converter<String, LocalDateTime> {

	@Override
	public LocalDateTime convert(String text) {
		LocalDateTime localDateTime = null;
		if (null != text && !text.isEmpty()) {
			text = LocalDateUtil.allToDateTimeMillisecond(text);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(LocalDateUtil.FORMAT_DATE_TIME_MILLISECOND);
			localDateTime = LocalDateTime.parse(text, formatter);
		}
		return localDateTime;
	}
}
