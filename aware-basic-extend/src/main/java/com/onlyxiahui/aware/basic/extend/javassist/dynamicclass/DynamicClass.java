package com.onlyxiahui.aware.basic.extend.javassist.dynamicclass;

/**
 * 
 * Description <br>
 * Date 2020-04-10 15:37:38<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class DynamicClass {

	private Class<?> targetClass;

	private Class<?> fieldClass;

	public Class<?> getTargetClass() {
		return targetClass;
	}

	public void setTargetClass(Class<?> targetClass) {
		this.targetClass = targetClass;
	}

	public Class<?> getFieldClass() {
		return fieldClass;
	}

	public void setFieldClass(Class<?> fieldClass) {
		this.fieldClass = fieldClass;
	}

}
