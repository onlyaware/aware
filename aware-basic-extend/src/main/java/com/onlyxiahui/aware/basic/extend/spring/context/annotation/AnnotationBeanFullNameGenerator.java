package com.onlyxiahui.aware.basic.extend.spring.context.annotation;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;

/**
 * Date 2019-01-06 18:18:07<br>
 * Description 用于设置spring生成对象时采用完整类名做id（含包名）
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class AnnotationBeanFullNameGenerator extends AnnotationBeanNameGenerator {

	@Override
	protected String buildDefaultBeanName(BeanDefinition definition) {
		String className = null != definition ? definition.getBeanClassName() : "";
		return className == null ? "" : className;
	}
}
