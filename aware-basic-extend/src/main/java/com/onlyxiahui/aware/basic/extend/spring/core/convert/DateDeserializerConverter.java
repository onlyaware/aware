package com.onlyxiahui.aware.basic.extend.spring.core.convert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * Description <br>
 * Date 2019-05-21 14:51:31<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class DateDeserializerConverter implements Converter<String, Date> {

	@Override
	public Date convert(String text) {
		Date date = null;
		if (null != text && !text.isEmpty()) {
			text = LocalDateUtil.allToDateTimeMillisecond(text);
			SimpleDateFormat df = new SimpleDateFormat(LocalDateUtil.FORMAT_DATE_TIME_MILLISECOND);
			try {
				date = df.parse(text);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return date;
	}
}
