package com.onlyxiahui.aware.basic.extend.fastjson.parser;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * Description <br>
 * Date 2019-06-20 22:41:30<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class FastjsonTimestampDeserializer implements ObjectDeserializer {

	@SuppressWarnings("unchecked")
	@Override
	public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
		// 如果是字符串格式
		String text = parser.getLexer().stringVal();
		parser.getLexer().nextToken();
		Timestamp timestamp = null;
		if (null != text) {
			String date = LocalDateUtil.allToDateTimeMillisecond(text);
			try {
				DateFormat dateFormat = new SimpleDateFormat(LocalDateUtil.FORMAT_DATE_TIME_MILLISECOND);
				Date localDate = dateFormat.parse(date);
				new Timestamp(localDate.getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return (T) timestamp;
	}

	@Override
	public int getFastMatchToken() {
		// TODO Auto-generated method stub
		return 0;
	}
}
