package com.onlyxiahui.aware.basic.extend.fastjson.parser;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * Description <br>
 * Date 2019-06-20 22:52:06<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class FastjsonLocalDateTimeSerializer implements ObjectSerializer {

	@Override
	public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features) throws IOException {
		// TODO Auto-generated method stub
		if (object instanceof LocalDateTime) {
			String date = DateTimeFormatter.ofPattern(LocalDateUtil.FORMAT_DATE_TIME).format((LocalDateTime) object);
			serializer.write(date);
		}
	}
}
