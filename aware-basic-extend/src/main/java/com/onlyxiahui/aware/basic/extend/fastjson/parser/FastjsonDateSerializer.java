package com.onlyxiahui.aware.basic.extend.fastjson.parser;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * Description <br>
 * Date 2019-06-20 22:52:06<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class FastjsonDateSerializer implements ObjectSerializer {

	@Override
	public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features) throws IOException {
		// TODO Auto-generated method stub
		if (object instanceof Date) {
			SimpleDateFormat dateFromat = new SimpleDateFormat();
			dateFromat.applyPattern(LocalDateUtil.FORMAT_DATE_TIME);
			String date = dateFromat.format((Date) object);
			serializer.write(date);
		}
	}
}
