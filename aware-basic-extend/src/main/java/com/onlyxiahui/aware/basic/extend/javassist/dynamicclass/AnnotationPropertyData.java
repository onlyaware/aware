package com.onlyxiahui.aware.basic.extend.javassist.dynamicclass;

/**
 * 
 * Description <br>
 * Date 2020-04-10 15:37:38<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class AnnotationPropertyData {

	private String name;
	private Object value;

	public AnnotationPropertyData() {
		super();
	}

	public AnnotationPropertyData(String name, Object value) {
		super();
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
