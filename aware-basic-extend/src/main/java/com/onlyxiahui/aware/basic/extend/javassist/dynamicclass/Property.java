package com.onlyxiahui.aware.basic.extend.javassist.dynamicclass;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * 
 * Description <br>
 * Date 2020-04-10 15:37:38<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class Property {

	/**
	 * 属性名称
	 */
	private String name;

	/**
	 * 属性类型
	 */
	private Class<?> dataClass = String.class;

	private List<Annotation> annotations;

	private List<AnnotationData> annotationDatas;

	private List<Property> nodes;

	public Property() {
	}

	public Property(String name) {
		super();
		this.name = name;
	}

	public Property(String name, Class<?> dataClass) {
		super();
		this.name = name;
		this.dataClass = dataClass;
	}

	public Property(String name, Class<?> dataClass, List<Annotation> annotations) {
		super();
		this.name = name;
		this.dataClass = dataClass;
		this.annotations = annotations;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Class<?> getDataClass() {
		return dataClass;
	}

	public void setDataClass(Class<?> dataClass) {
		this.dataClass = dataClass;
	}

	public List<Annotation> getAnnotations() {
		return annotations;
	}

	public void setAnnotations(List<Annotation> annotations) {
		this.annotations = annotations;
	}

	public List<Property> getNodes() {
		return nodes;
	}

	public void setNodes(List<Property> nodes) {
		this.nodes = nodes;
	}

	public List<AnnotationData> getAnnotationDatas() {
		return annotationDatas;
	}

	public void setAnnotationDatas(List<AnnotationData> annotationDatas) {
		this.annotationDatas = annotationDatas;
	}
}
