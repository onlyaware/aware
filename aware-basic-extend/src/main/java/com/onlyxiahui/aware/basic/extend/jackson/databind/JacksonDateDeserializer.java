package com.onlyxiahui.aware.basic.extend.jackson.databind;

import java.io.IOException;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.onlyxiahui.common.utils.base.util.time.DateUtil;
import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * 
 * Description 时间格式反序列化 <br>
 * Date 2019-04-29 09:18:16<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class JacksonDateDeserializer extends JsonDeserializer<Date> {
	@Override
	public Date deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		Date localDate = null;
		String text = jsonParser.getText();
		if (null != text) {
			String date = LocalDateUtil.allToDateTimeMillisecond(text);
			localDate = DateUtil.stringDateToDate(date, LocalDateUtil.FORMAT_DATE_TIME_MILLISECOND);
		}
		return localDate;
	}
}
