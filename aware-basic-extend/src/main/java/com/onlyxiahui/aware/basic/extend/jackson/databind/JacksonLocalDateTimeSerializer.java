package com.onlyxiahui.aware.basic.extend.jackson.databind;

import java.io.IOException;
import java.time.LocalDateTime;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * 
 * Description 时间格式序列化 <br>
 * Date 2019-04-29 09:27:32<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class JacksonLocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {

	@Override
	public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
		String date = LocalDateUtil.format(localDateTime, LocalDateUtil.FORMAT_DATE_TIME);
		jsonGenerator.writeString(date);
	}
}
