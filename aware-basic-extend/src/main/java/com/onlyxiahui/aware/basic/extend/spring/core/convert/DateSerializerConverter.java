package com.onlyxiahui.aware.basic.extend.spring.core.convert;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * Description <br>
 * Date 2019-05-21 14:52:08<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class DateSerializerConverter implements Converter<Date, String> {

	@Override
	public String convert(Date date) {
		String text = null;
		if (null != date) {
			SimpleDateFormat df = new SimpleDateFormat(LocalDateUtil.FORMAT_DATE_TIME_MILLISECOND);
			text = df.format(date);
		}
		return text;
	}
}
