package com.onlyxiahui.aware.basic.extend.spring.context.util;

import javax.annotation.Resource;

import org.springframework.context.ApplicationContext;

/**
 * Date 2019-01-06 18:19:15<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */
public class SpringUtil {

	private static ApplicationContext applicationContext = null;

	@Resource
	public void setApplicationContext(ApplicationContext applicationContext) {
		SpringUtil.applicationContext = applicationContext;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getBean(String name) {
		Object o = null;
		if (null != applicationContext) {
			o = applicationContext.getBean(name);
		}
		return (T) o;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getBean(Class<T> type) {
		Object o = null;
		if (null != applicationContext) {
			o = applicationContext.getBean(type);
		}
		return (T) o;
	}

	public static String[] getBeanNamesForType(Class<?> type) {
		String[] as = null;
		if (null != applicationContext) {
			as = applicationContext.getBeanNamesForType(type);
		}
		return as;
	}
}
