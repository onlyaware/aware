package com.onlyxiahui.aware.basic.extend.jackson.databind;

import java.io.IOException;
import java.time.LocalDate;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.onlyxiahui.common.utils.base.util.time.LocalDateUtil;

/**
 * 
 * Description 时间格式序列化 <br>
 * Date 2019-04-29 09:25:18<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
public class JacksonLocalDateSerializer extends JsonSerializer<LocalDate> {
	@Override
	public void serialize(LocalDate value, JsonGenerator jsonGenerator, SerializerProvider serializers) throws IOException {
		String date = LocalDateUtil.format(value, LocalDateUtil.FORMAT_DATE);
		// Long time =
		// value.atStartOfDay().toInstant(ZoneOffset.of("+8")).toEpochMilli() / 1000;
		jsonGenerator.writeString(date);
	}
}
