package com.test;

import java.util.ArrayList;
import java.util.List;

/**
 * Description <br>
 * Date 2020-12-24 17:07:40<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class Data {

	private String code;
	private String name;

	private List<Data> nodes;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Data> getNodes() {
		return nodes;
	}

	public void setNodes(List<Data> nodes) {
		this.nodes = nodes;
	}

	public void add(Data data) {
		if (null == this.nodes) {
			this.nodes = new ArrayList<>();
		}
		this.nodes.add(data);
	}
}
