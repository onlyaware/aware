package com.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.onlyxiahui.common.utils.base.lang.string.StringUtil;

/**
 * Description <br>
 * Date 2020-11-28 15:30:29<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class Ctiy {

	List<Data> countryList = new ArrayList<>();

	public static void main(String[] args) {

		Ctiy cd = new Ctiy();
		System.out.println("--------------------");
		cd.country((s) -> {
			System.out.println(s.getCode() + "|" + s.getName());
		});

		System.out.println("--------------------");
		cd.state((countryCode, s) -> {
			System.out.println(countryCode + "->" + s.getCode() + "|" + s.getName());
		});

		System.out.println("--------------------");
		cd.city((countryCode, s, c) -> {
			System.out.println(countryCode + "->" + s + "->" + c.getCode() + "|" + c.getName());
		});
	}

	public Ctiy() {
		code();
	}

	public void code() {
		try {
			InputStream input = new FileInputStream(new File("doc/LocList.xml"));
			SAXReader saxReader = new SAXReader();
			Document document = saxReader.read(input);
			code(document);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void code(Document document) {
		Element rootElement = document.getRootElement();
		List<Element> list = rootElement.elements();
		if (null != list && !list.isEmpty()) {
			for (Element element : list) {
				country(element);
			}
		}
	}

	public void country(Element e) {
		if (null != e) {
			String name = e.attributeValue("Name");
			String code = e.attributeValue("Code");

			StringBuilder sb = new StringBuilder();
			sb.append("Country:");
			sb.append(name);
			sb.append("-");
			sb.append(code);
			System.out.println(sb);

			if (StringUtil.isNotBlank(code) && StringUtil.isNotBlank(name) && !"1".equals(code)) {
				Data d = new Data();
				d.setCode(code);
				d.setName(name);

				countryList.add(d);

				List<Element> es = e.elements();
				if (null != es) {
					for (Element ei : es) {
						state(d, ei);
					}
				}
			}
		}
	}

	public void state(Data country, Element e) {
		if (null != e) {
			String name = e.attributeValue("Name");
			String code = e.attributeValue("Code");

			StringBuilder sb = new StringBuilder();
			sb.append("-");
			sb.append("State:");
			sb.append(name);
			sb.append("-");
			sb.append(code);
			System.out.println(sb);

			if (null == code || code.isEmpty()) {
				code = "0";
			}

			if (null == name) {
				name = "";
			}

			Data d = new Data();
			d.setCode(code);
			d.setName(name);

			country.add(d);

			List<Element> es = e.elements();
			if (null != es) {
				for (Element ei : es) {
					city(d, ei);
				}
			}
		}
	}

	public void city(Data state, Element e) {
		if (null != e) {
			String name = e.attributeValue("Name");
			String code = e.attributeValue("Code");
			if (StringUtil.isNotBlank(code) && StringUtil.isNotBlank(name)) {
				Data d = new Data();
				d.setCode(code);
				d.setName(name);
				state.add(d);
			}
			StringBuilder sb = new StringBuilder();
			sb.append("-");
			sb.append("-");
			sb.append("City:");
			sb.append(name);
			sb.append("-");
			sb.append(code);
			System.out.println(sb);
		}
	}

	public void country(CountryStream stream) {
		if (null != stream) {
			for (Data d : countryList) {
				stream.handle(d);
			}
		}
	}

	public void state(StateStream stream) {
		if (null != stream) {
			for (Data d : countryList) {
				if (d.getNodes() != null) {
					for (Data s : d.getNodes()) {
						if (!"0".equals(s.getCode()) && StringUtil.isNotBlank(s.getName())) {
							stream.handle(d.getCode(), s);
						}
					}
				}
			}
		}
	}

	public void city(CtiyStream stream) {
		if (null != stream) {
			for (Data d : countryList) {
				if (d.getNodes() != null) {
					for (Data s : d.getNodes()) {
						if (s.getNodes() != null) {
							for (Data c : s.getNodes()) {
								if (StringUtil.isNotBlank(c.getCode()) && StringUtil.isNotBlank(c.getName())) {
									stream.handle(d.getCode(), s.getCode(), c);
								}
							}
						}
					}
				}
			}
		}
	}
}
