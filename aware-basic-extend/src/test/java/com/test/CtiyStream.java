package com.test;

/**
 * Description <br>
 * Date 2020-12-24 17:08:46<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public interface CtiyStream {

	void handle(String countryCode, String stateCode, Data ctiy);
}
