package com.onlyxiahui.aware.extend.spring.cloud.feign.codec;

import org.springframework.context.annotation.Bean;

import feign.codec.Decoder;
import feign.codec.Encoder;

/**
 * 
 * <br>
 * Date 2020-05-13 21:03:51<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
//@Configuration
public class FeignConfiguration {

	@Bean
	public Encoder feignEncoder() {
		return new DataEncoder();
	}

	@Bean
	public Decoder feignDecoder() {
		return new DataDecoder();
	}
}
