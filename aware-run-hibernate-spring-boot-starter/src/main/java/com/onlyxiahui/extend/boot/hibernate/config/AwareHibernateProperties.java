package com.onlyxiahui.extend.boot.hibernate.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 
 * Description <br>
 * Date 2020-11-06 15:54:28<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = "spring.aware.hibernate")
public class AwareHibernateProperties {

	private Map<String, String> setting = new HashMap<>();
	private String[] entityPackages;
	private String[] queryPaths;
	private String namingStrategy = "";

	public Map<String, String> getSetting() {
		return setting;
	}

	public void setSetting(Map<String, String> setting) {
		this.setting = setting;
	}

	public String[] getEntityPackages() {
		return entityPackages;
	}

	public void setEntityPackages(String[] entityPackages) {
		this.entityPackages = entityPackages;
	}

	public String[] getQueryPaths() {
		return queryPaths;
	}

	public void setQueryPaths(String[] queryPaths) {
		this.queryPaths = queryPaths;
	}

	public String getNamingStrategy() {
		return namingStrategy;
	}

	public void setNamingStrategy(String namingStrategy) {
		this.namingStrategy = namingStrategy;
	}
}
