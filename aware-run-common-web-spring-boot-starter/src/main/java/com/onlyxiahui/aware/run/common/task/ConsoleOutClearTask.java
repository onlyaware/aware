package com.onlyxiahui.aware.run.common.task;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 
 * date 2018-05-23 15:19:35<br>
 * description 清除输出文件内容
 * 
 * @author XiaHui<br>
 * @since
 */
@EnableScheduling
@Component
public class ConsoleOutClearTask {

	protected final Logger log = LogManager.getLogger(this.getClass());

	@Value("${spring.aware.system.config.out-file.path:logs/nohup.out}")
	String filePath = "logs/nohup.out";

	/**
	 * 清理linux下面输出文件越来越大
	 */
	@Scheduled(cron = "${spring.aware.system.config.out-file.clear-cron:0 0 0 */30 * ? }")
	public void outClear() {

		File file = new File(filePath);
		log.info("out-file clear:" + file.getAbsolutePath());
		log.info("out-file exists:" + file.exists());
		log.info("out-file absolute path:" + file.getAbsolutePath());

		if (file.exists()) {
			try {
				FileWriter fw = new FileWriter(file);
				fw.write("");
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
