package com.onlyxiahui.aware.run.common.starter;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.onlyxiahui.aware.run.common.log4j2.Log4j2Refresh;
import com.onlyxiahui.aware.run.common.task.ConsoleOutClearTask;

/**
 * 
 * <br>
 * Date 2020-06-12 17:15:39<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@Import({
		Log4j2Refresh.class,
		ConsoleOutClearTask.class })
@Configuration
public class ServerRunStartConfig {

}
